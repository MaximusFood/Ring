//
//  AdFrameworkController.h
//  AdManager
//
//  Created by Maxim Arnold on 21/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//
//  Each framework has one of these.
//

#import <Foundation/Foundation.h>
#import "AdManager.h"


@interface AdFrameworkController : NSObject
{
    NSString* m_frameworkType;
    
    // Some frameworks don't let us know immediately, when we attempt to show an ad,
    // whether or not the ad will actually be shown. In this case we'll be setting our
    // ad shown flag to true incorrectly. So if the framework is the first one to be
    // switching that flag, make a note, and then we can switch it back if the failure
    // delegate method is called. 
    BOOL m_firstAdShown;
}


@property (nonatomic, strong, readonly) NSString* frameworkType;

@end
