//
//  FlurryController.h
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import "Flurry.h"
#import "FlurryAdInterstitial.h"
#import "FlurryAdInterstitialDelegate.h"


@interface FlurryController : AdFrameworkController <FlurryAdInterstitialDelegate>

@property (nonatomic, strong) NSString* apiKey;
@property (nonatomic, strong) NSString* adSpaceName;
@property (nonatomic) BOOL testMode;

+ (id)controller;

@end
