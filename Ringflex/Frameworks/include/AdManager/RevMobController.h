//
//  FlurryController.h
//  AdManager
//
//  Created by Maxim Arnold on 22/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import "AdFrameworkController.h"
#import <RevMobAds/RevMobAds.h>


@interface RevMobController : AdFrameworkController <RevMobAdsDelegate>

@property (nonatomic, strong) NSString* mediaID;


+ (id)controller;

@end
