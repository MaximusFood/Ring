//
//  Ring.swift
//  Ringflex
//
//  Created by Maxim Arnold on 30/10/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

import SpriteKit

class Ring : SKNode {
    
    /*
    *   MARK: Public Properties (and read-only pubs)
    */
    
    private(set) var ringSize = 0.0
    private(set) var ringTargetSize = 0.0
    
    // As thing ring is going to redraw itself, it needs to know what it's supposed to
    // look like
    var fillColor = GameColours.kTransparent
    var strokeColor = GameColours.kGrey
    var lineWidth = 5
    
    var isTargetSize: Bool {
        return ringSize == ringTargetSize
    }
    
    
    /*
    *   MARK: Private Properties
    */
    
    private var ringShape: SKShapeNode
    
    private var ringGrowSpeed = 0.0
    
    private var updateFunction: (Double) -> Double = {$0}
    
    // Animation-related vars
    private var animationStartSize = 0.0
    private var animationStartTime = 0.0
    
    
    /*
    *   MARK: Initialisers
    */
    
    override init() {
        ringShape = SKShapeNode(ellipseOfSize: CGSize(width: 200, height: 200))
        
        super.init()
        
        addChild(ringShape)
        
        setLinearUpdateFunc()
    }

    /*
    *   Required for silly Swift
    */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    *   MARK: Public Methods
    */
    
    /*
    *   This is the big one. We set a new target size for the ring, alond with a start
    *   time, and either a speed or a duration, as well as the type of move action (i.e.
    *   linear or eased). After this the updateRingSize method is called with a current
    *   time, and this is used to redraw the ring.
    */
    func setNewTargetSize(target: Double,
        startTime: Double,
        speed: Double = 0.0,
        duration: Double = 0.0,
        easeAction: Bool = false) {
            
        // Set which type of update function to use
        if easeAction {
            setEaseInOutUpdateFunc()
        } else {
            setLinearUpdateFunc()
        }
        
        ringTargetSize = target
        animationStartSize = ringSize
        animationStartTime = startTime
        
        // If we have a duration then use it to work out what the speed should be
        if duration > 0.0 {
            // if a duration has been passed, set the speed accordingly
            ringGrowSpeed = abs(ringTargetSize - ringSize) / duration
        // Othereise just use the speed.
        } else if speed > 0.0 {
            ringGrowSpeed = speed
        }
    }
    
    /*
    *   This should be called from teh game's main loop
    */
    func updateRingSize(withCurrentTime currentTime: Double) {
        // Don't bother doing anything if we are already our target size.
        if !isTargetSize {
            // We calculate the new size using the update funciton and the time...
            ringSize = updateFunction(currentTime - animationStartTime)
            // ... then redraw the ring shape
            redrawRing()
        }
    }
    
    
    /*
    *   Give the outside world a chance to redraw the ring without animation
    */
    func redrawRing(withSize size: Double) {
        ringSize = size
        ringTargetSize = size
        redrawRing()
    }
    
    
    /*
    *   MARK: Private Helper Methods
    */
    
    /*
    *   Remove the ring shape from the Ring node, and add it again with a new size.
    */
    private func redrawRing() {
        ringShape.removeFromParent()
        drawRing()
    }
    
    private func drawRing() {
        ringShape = SKShapeNode(ellipseOfSize: CGSize(width: ringSize, height: ringSize))
        ringShape.fillColor = fillColor
        ringShape.strokeColor = strokeColor
        ringShape.lineWidth = CGFloat(lineWidth)
        addChild(ringShape)
    }
    
    
    /*
    *   The following two methods set the updateFunction variable. We have either a simple
    *   linear option, or a quadratic ease in out option.
    */
    
    private func setEaseInOutUpdateFunc() {
        updateFunction = { (currentTime: Double) -> Double in
            let b = self.animationStartSize
            let c = self.ringTargetSize - self.animationStartSize
            let d = abs(c)/self.ringGrowSpeed
            var t = currentTime
            
            // hmm having some problems. This should sort it.
            if currentTime > d {
                return self.ringTargetSize
            }
            
            t /= d/2
            if t < 1 { return c/2*t*t + b }
            t--
            return -c/2 * (t*(t-2) - 1) + b
        }
    }
    
    private func setLinearUpdateFunc() {
        updateFunction = { (currentTime: Double) -> Double in
            let b = self.animationStartSize
            let c = self.ringTargetSize - self.animationStartSize
            let d = abs(c)/self.ringGrowSpeed
            let t = currentTime
            
            if currentTime > d {
                return self.ringTargetSize
            }
            
            return c*t/d + b;
        }
    }
}
