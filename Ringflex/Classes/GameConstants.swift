//
//  GameConstants.swift
//  Ringflex
//
//  Created by Maxim Arnold on 19/09/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

import SpriteKit

// For a game of this size it is helpful to have a bunch of static constants for certain things, like colours, fonts, etc.

// Colours used across the game
struct GameColours {
    static let kBlue = SKColor(red:0.525, green:0.773, blue:0.859, alpha: 1)
    static let kGrey = SKColor(red:0.286, green:0.286, blue:0.286, alpha:1)
    static let kLightGrey = SKColor(red:0.65, green:0.65, blue:0.65, alpha:1)
    static let kWhite = SKColor(red:0.96, green:0.96, blue:0.89, alpha:1)
    static let kRed = SKColor(red:0.878, green:0.384, blue:0.318, alpha:1)
    static let kTransparent = SKColor(red:1, green:1, blue:1, alpha:0)
}

// Font
let kGameFont = "Open Sans Bold"
