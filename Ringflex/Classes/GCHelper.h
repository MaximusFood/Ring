//
//  GCHelper.h
//  v0.1.0
//
//  Created by Ray Wenderlich on 4/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>

#define kLeaderboardIDScore @"RingflexTopScores"

@interface GCHelper : NSObject  <GKGameCenterControllerDelegate>
{
    BOOL m_gameCenterAvailable;
    BOOL m_userAuthenticated;
    
    UIViewController* m_uiViewController;
}

@property (assign, readonly) BOOL gameCenterAvailable;
@property (strong, nonatomic) UIViewController* uiViewController;


+ (GCHelper *)sharedInstance;

- (void)authenticateLocalUser;

- (void)reportScore:(int64_t)score;

- (void)showLeaderboard;


@end
