//
//  SettingsLayer.swift
//  Ringflex
//
//  Created by Maxim Arnold on 08/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

import SpriteKit

class SettingsLayer: SKNode, IAPHelperObserver {
    
    /*
    *   Properties
    */
    
    var buttonHighlightColour: SKColor
    
    private let bg = SKSpriteNode(imageNamed: "settingsbg")
    private var gameScene: GameScene
    
    
    /*
    *   Initialisation
    */
    
    init(gameScene: GameScene, buttonHighlightColour: SKColor) {
        self.gameScene = gameScene
        self.buttonHighlightColour = buttonHighlightColour
        
        super.init()
        
        bg.color = GameColours.kWhite
        bg.colorBlendFactor = 1.0
        addChild(bg)
        
        addButtons()
        
        userInteractionEnabled = true
        
        // Register self as an observer of the IAPHelper
        IAPHelper.sharedInstance().addAdRemovalObserver(self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    *   MARK: User Interaction Methods
    */
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        // Region is based on the size of the visible background. For simplicity the
        // actual image is much larger. This rather dodgy.
        let menuRegion = CGRect(x: -125,
            y: -125,
            width: 250,
            height: 250)
        
        // If touch is outside the bg, remove menu
        for touch in touches {
            if !CGRectContainsPoint(menuRegion, touch.locationInNode(self)) {
                removeSettingsLayer()
            }
        }
    }
    
    
    /*
    *   MARK: Private Helpers
    */
    
    private func addButtons(adsRemoved: Bool = false) {
        let button = { (text: String, action: () -> Void, yPos: CGFloat) in
            let b = MXTextButton(buttonText: text, buttonAction: action)
            b.buttonLabel.fontColor = GameColours.kGrey
            b.buttonLabel.fontName = kGameFont
            b.buttonLabel.fontSize = 28
            b.highlightColour = self.buttonHighlightColour
            b.position = CGPoint(x: 0, y: yPos)
            self.addChild(b)
            b.name = "button"
        }
        
        if GameManager.sharedInstance.adsRemoved || adsRemoved {
            button("ABOUT", onAboutPb, 30)
            button("HOW TO PLAY", onHowToPlayPb, -30)
        } else {
            button("ABOUT", onAboutPb, 75)
            button("HOW TO PLAY", onHowToPlayPb, 25)
            button("REMOVE ADS", onRemoveAdsPb, -25)
            button("RESTORE", onRestorePurchasePb, -75)
        }
    }
    
    /*
    *   Adds the about screen, with the buttons.
    */
    private func addAboutButtons() {
        let button = { (text: String, action: () -> Void, yPos: CGFloat) in
            let b = MXTextButton(buttonText: text, buttonAction: action)
            b.buttonLabel.fontColor = GameColours.kGrey
            b.buttonLabel.fontName = kGameFont
            b.buttonLabel.fontSize = 22
            b.highlightColour = self.buttonHighlightColour
            b.position = CGPoint(x: 0, y: yPos)
            self.addChild(b)
        }
        
        let heading = { (text: String, yPos: CGFloat) in
            let h = SKLabelNode(fontNamed: kGameFont)
            h.text = text
            h.fontColor = GameColours.kLightGrey
            h.verticalAlignmentMode = .Center
            h.horizontalAlignmentMode = .Center
            h.fontSize = 15
            h.position = CGPoint(x: 0, y: yPos)
            self.addChild(h)
        }
        
        let label = { (text: String, yPos: CGFloat) in
            let h = SKLabelNode(fontNamed: kGameFont)
            h.text = text
            h.fontColor = GameColours.kGrey
            h.verticalAlignmentMode = .Center
            h.horizontalAlignmentMode = .Center
            h.fontSize = 22
            h.position = CGPoint(x: 0, y: yPos)
            self.addChild(h)
        }
        
        heading("CODE AND STUFF", 65)
        label("MAXIM ARNOLD", 40)
        heading("ON THE APP STORE", -10)
        button("RATE THIS APP!", onRatePb, -35)
        button("MORE FROM MAXIM", onMoreAppsPb, -64)
        
    }
    
    private func removeButtons() {
        for n in self.children {
            if n.name == "button" {
                n.removeFromParent()
            }
        }
    }
    
    /*
    *   MARK: Button Actions
    */
    
    /*
    *   Main Button Actions
    */
    
    private func onHowToPlayPb() {
        let alert = UIAlertView(title: "How To Play!",
            message: "Chase the white ring as it grows and contracts, with the dark ring "
                + "moving at an ever increasing pace. Simply tap the screen when the two "
                + "rings are aligned.  You'll need world-class reflexes as the intensity "
                + "builds and builds!",
            delegate: nil,
            cancelButtonTitle: "Got It!")
        alert.show()
        
        Flurry.logEvent("How to Play Button Pressed")
    }
    
    private func onAboutPb() {
        removeButtons()
        addAboutButtons()
        
        Flurry.logEvent("About Button Pressed")
    }
    
    private func onRemoveAdsPb() {
        IAPHelper.sharedInstance().tapsRemoveAds()
        
        Flurry.logEvent("Remove Ads Button Pressed")
    }
    
    private func onRestorePurchasePb() {
        IAPHelper.sharedInstance().restore()
        
        Flurry.logEvent("Restore Button Pressed")
    }
    
    /*
    *   About Button Actions
    */
    
    private func onRatePb() {
        let iTunesLink = NSURL(string: "itms://itunes.apple.com/gb/app/ringflex/id1053880983?mt=8")
        if let actualLink = iTunesLink {
            UIApplication.sharedApplication().openURL(actualLink)
        }
        
        Flurry.logEvent("Rate Button Pressed")
    }
    
    private func onMoreAppsPb() {
        let iTunesLink = NSURL(string: "itms://itunes.apple.com/gb/artist/maxim-buckingham-arnold/id431856800")
        if let actualLink = iTunesLink {
            UIApplication.sharedApplication().openURL(actualLink)
        }
        
        Flurry.logEvent("More Button Pressed")
    }
    
    private func removeSettingsLayer() {
        self.gameScene.settingsMenuRemoved()
        IAPHelper.sharedInstance().removeAdRemovalObserver(self)
        self.growOut(withDuration: 0.12)
    }
    
    /*
    *   MARK: IAPAdRemovalListener Methods
    *   We make SettingsLayer an ad removal listener so that we can react once the ads
    *   have been removed, and remove the related buttons.
    */
    
    @objc func removeAdsPurchased() {
        // Reload the buttons, removing the remove ads and restore buttons
        removeButtons()
        addButtons(true)
    }
}
