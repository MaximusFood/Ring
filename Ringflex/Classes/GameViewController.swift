//
//  GameViewController.swift
//  Ringflex
//
//  Created by Maxim Arnold on 18/09/2015.
//  Copyright (c) 2015 Maxim. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Get IDs and stuff from a config file
        let path = NSBundle.mainBundle().pathForResource("config", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        
        // Set up AdManager
        AdManager.sharedInstance().uiViewController = self
        
        AdManager.sharedInstance().chartboostController.cbAppID = dict!.valueForKey("cbAppID") as! String
        AdManager.sharedInstance().chartboostController.cbAppSig = dict!.valueForKey("cbAppSig") as! String
        
        AdManager.sharedInstance().adMobController.adMobUnitID = dict!.valueForKey("adMobUnitID") as! String
        AdManager.sharedInstance().adMobController.adMobTestDevices = [dict!.valueForKey("myiPhoneID") as! String]
        
        AdManager.sharedInstance().flurryController.apiKey = dict!.valueForKey("flurryID") as! String
        AdManager.sharedInstance().flurryController.adSpaceName = dict!.valueForKey("flurryAdSpaceName") as! String
        
        AdManager.sharedInstance().vungleController.appID = dict!.valueForKey("vungleAppID") as! String
        
        AdManager.sharedInstance().revMobController.mediaID = dict!.valueForKey("revMobMediaID") as! String
        
        AdManager.sharedInstance().chanceOfAttempt = 0.8
        AdManager.sharedInstance().cooldownAttempts = 2
        
        AdManager.sharedInstance().priorityList = [kFrameworkTypeChartboost : 4,
                                                    kFrameworkTypeFlurry : 4,
                                                    kFrameworkTypeAdMob : 2,
                                                    kFrameworkTypeIAd : 1,
                                                    kFrameworkTypeRevMob : 4,
                                                    kFrameworkTypeVungle: 4]
        
        AdManager.sharedInstance().prepareAdFrameworks()
        
        // This will basically kill the ad manager if the user has purchased RemoveAds (or
        // if they've hacked up NSUserDefaults).
        AdManager.sharedInstance().suppressAllAds = GameManager.sharedInstance.adsRemoved
        
        // Set up GC Helper
        GCHelper.sharedInstance().uiViewController = self
        GCHelper.sharedInstance().authenticateLocalUser()
        
        // And now prepare the scene
        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = false
            skView.showsNodeCount = false
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            scene.size = skView.frame.size
            
            // Must do this before we presentScene
            SKNode.Hack.contentSize = scene.size
            
            skView.presentScene(scene)
        }
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
