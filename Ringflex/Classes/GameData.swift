//
//  GameData.swift
//  Ringflex
//
//  Created by Maxim Arnold on 28/10/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

import Foundation

class GameData {
    
    /*
    *   MARK: Read-Only vars
    */
    private(set) var score = 0
    private(set) var highScore = 0
    private(set) var newHighScore = false
    
    
    /*
    *   MARK: Initialisation
    */
    
    init() {
        // Load up the high score
        highScore = GameManager.sharedInstance.loadHighScore()
        
        resetGameData()
    }
    
    
    /*
    *   MARK: Public Methods
    */
    
    func incScore() {
        score++
    }
    
    func gameOver() {
        // Persist if new high score
        if score > highScore {
            newHighScore = true
            
            highScore = score
            GameManager.sharedInstance.persistHighScore(highScore)
        }
        
        // Always report to game centre
        GCHelper.sharedInstance().reportScore(Int64(score))
    }
    
    func resetGameData() {
        score = 0
        newHighScore = false
    }
}