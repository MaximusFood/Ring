//
//  GameScene.swift
//  Ringflex
//
//  Created by Maxim Arnold on 18/09/2015.
//  Copyright (c) 2015 Maxim. All rights reserved.
//

import SpriteKit

/*
*   Game State
*/
enum GameState: Int {
    case Running = 0, Resetting, Ready, InitialReady, Failed, FailedWait
}


class GameScene: SKScene {
    
    /*
    *   MARK: Scene Properties
    */
    
    // Background sprite, rather than scene's backgroundColour property, allows us to fade
    // a colour change
    private let background = SKSpriteNode()
    
    // Add stuff to this, add this to the scene. Then we can blur everything on settings.
    private let gameLayer = SKEffectNode()
    
    // Labels
    private let scoreLabel = SKLabelNode(fontNamed: kGameFont)
    private let highScoreLabel = SKLabelNode(fontNamed: kGameFont)
    private let highScoreLabelText = SKLabelNode(fontNamed: kGameFont)
    
    // Object that keeps hold of scores and stuff
    private let gameData = GameData()
    
    // Buttons
    private let gcButton = MXImageButton(buttonImage:"gcicon")
    private let settingsButton = MXImageButton(buttonImage:"settingsicon")
    
    // The two rings: Hunter and Hunted, Jager and Quarry.
    private var jagerRing = Ring()
    private var quarryRing = Ring()

    // Various game flags and variables.
    private var lastTime: Double = 0.0
    
    private var gameState: GameState = .InitialReady
    
    private var initJagerSize = 300.0
    private var initQuarrySize = 150.0
    
    private var resetDuration = 0.3
    
    /*
    *   EASTER EGG - GOSLING MODE
    */
    private var goslingTaps = ""
    private var goslingModeActivated = false
    private var goslingShockedLimit = 120
    private var goslingShocked = false
    
    
    /*
    *   MARK: Initialisation
    */
    
    // Well, initialisation for all intents and purposes.
    override func didMoveToView(view: SKView) {
        // Setup bg
        background.size = self.size
        background.color = GameColours.kBlue
        background.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        addChild(background)
        
        // Set up gameLayer
        addGameLayer()
    
        // Set up Quarry ring
        quarryRing.fillColor = GameColours.kTransparent
        quarryRing.strokeColor = GameColours.kWhite
        quarryRing.lineWidth = 21
        quarryRing.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        gameLayer.addChild(quarryRing)
        
        // Set up Jager ring
        jagerRing.fillColor = GameColours.kTransparent
        jagerRing.strokeColor = GameColours.kGrey
        jagerRing.lineWidth = 7
        jagerRing.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        gameLayer.addChild(jagerRing)
        
        // And draw the rings at their initial positions
        jagerRing.redrawRing(withSize: initJagerSize)
        quarryRing.redrawRing(withSize: initQuarrySize)
        
        // Draw the labels and buttons
        addScoreLabel()
        addButtons()
        addBestScoreLabel()
        
        // Can't set the button action at declaration time for some reason.
        gcButton.action = gameCentrePb
        settingsButton.action = settingsPb
    }
    
    
    /*
    *   MARK: User Interaction
    */
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /*
        *   EASTER EGG - GOSLING MODE
        *   First of all, we've got to log the touch, for gosling mode!
        */
        checkGoslingModeActivated(touches)
        
        // Ignore all touches when resetting, or when in FailedWait state.
        if gameState == .Resetting || gameState == .FailedWait {
            return
        }
        
        // If the game isn't running, we first need a touch to reset the game, then
        // another to kick off a new one.
        if gameState != .Running {
            if gameState == .Ready || gameState == .InitialReady {
                startLevel()
            } else {
                readyLevel()
            }
            return
        }
        
        // Otherwise we're talking about an in-game touch
        if isJagerTouchingQuarry() {
            successfulHunt()
        } else {
            levelFailed()
        }
    }
   
    
    /*
    *   MARK: Game Loop
    */
    
    override func update(currentTime: CFTimeInterval) {
        
        if gameState == .Running || gameState == .Resetting {
            // We will have setup a new target size for these rings somewhere else, here
            // we just call the update size methods, and pass the current time. The Ring
            // class does the rest.
            jagerRing.updateRingSize(withCurrentTime: currentTime)
            quarryRing.updateRingSize(withCurrentTime: currentTime)

            // The target size for the jager ring is the boundary size, so if it is this
            // size, we've failed
            if gameState == .Running && jagerRing.isTargetSize {
                levelFailed()
            }
        }
        
        /*
        *   EASTER EGG - GOSLING MODE
        */
        if goslingModeActivated {
            if goslingShocked && quarryRing.ringSize > Double(goslingShockedLimit) {
                reloadGosling()
            } else if !goslingShocked && quarryRing.ringSize < Double(goslingShockedLimit) {
                reloadGosling()
            }
        }
        
        //
        lastTime = currentTime
    }
    
    
    /*
    *   MARK: Public Methods
    */
    
    /*
    *   Called from settings layer when it is removed. Turns off the blur.
    */
    func settingsMenuRemoved() {
        gameLayer.shouldEnableEffects = false
    }
    
    
    /*
    *   MARK: Helper Methods
    */
    
    /*
    *   Called when user has tapped while the jager is touching its quarry: a succeddful 
    *   hunt.
    */
    private func successfulHunt() {
        gameData.incScore()
        scoreLabel.text = String(gameData.score)
        setNewRandomQuarrySize()
        changeJagerGrowDirection()
    }
    
    /*
    *   Called on a wayward tap, or when jager has grown/shrunk to the boundaries
    */
    private func levelFailed() {
        // Set the bg a shocking red of failure
        background.color = GameColours.kRed
        
        // Tell the gameData class what's going on.
        gameData.gameOver()
        
        // Set status to .FailedWait, so we have a short cooldown period before we can
        // reset the game. Otherwise the user may tap just after failing, and barely see
        // the red failed state. Confusing.
        gameState = .FailedWait
        runBlock({self.gameState = .Failed}, delay: 0.3)
        
        // Show the best score and buttons at this point
        addBestScoreLabel()
        addButtons()
        
        // Maybe show an ad! But not if user has just got a new high score; don't want to
        // sour such an achievement with an annoying ad.
        // Also, don't show an ad if the score is 0: it gets in the way of Gosling mode.
        if !gameData.newHighScore && gameData.score > 0 {
            AdManager.sharedInstance().attemptAdIfReady()
        }
    }
    
    private func readyLevel() {
        // While we're running the resetting animations, the state must be resetting.
        gameState = .Resetting
        self.background.runAction(SKAction.colorizeWithColor(GameColours.kBlue, colorBlendFactor: 1.0, duration: resetDuration))
        
        removeBestScoreLabel()
        removeButtons()
        
        resetGameValues()
        
        // Pop the flag to Ready once the animations have completed.
        runBlock({self.gameState = .Ready}, delay:resetDuration)
    }
    
    private func startLevel() {
        // The first time the game is loaded up the labels/buttons are all there, even
        // though we're in a ready state. We therefore have a special 'InitialReady' state
        // so we can know that we need to remove the buttons as the game beigns
        if gameState == .InitialReady {
            removeBestScoreLabel()
            removeButtons()
        }
        
        // Kick off the game..
        gameState = .Running
        
        // And start the Jager ring shrinking
        shrinkJagerRing()
        
        // And let the GameManager do what it needs to do
        GameManager.sharedInstance.logNewGame()
    }
    
    private func resetGameValues() {
        // Move the rings back into position. This time they're both being 'eased'
        jagerRing.setNewTargetSize(initJagerSize, startTime: lastTime, duration: resetDuration, easeAction: true)
        quarryRing.setNewTargetSize(initQuarrySize, startTime: lastTime, duration: resetDuration, easeAction: true)
        
        scoreLabel.text = "0"
        
        gameData.resetGameData()
    }


    /*
    *   Chooses a new quarry size, and sets up the resize action
    */
    private func setNewRandomQuarrySize() {
        var targetQuarrySize = 0.0
        
        // Choose a new random quarry size, but keep trying until it's a certain distance
        // away from the jager ring.
        repeat {
            targetQuarrySize = Double(Int.random(50...Int(self.size.width-22)))
        } while abs(targetQuarrySize-jagerRing.ringSize) < 110 // not within 110 of jager
        
        // Then set the new target size on the quarryRing. This means that each time
        // the update method is called on the ring in the game loop, the ring will be 
        // resized until it reaches the target size, according to the passed speed. Also,
        // by passing easeAction = true, we use a quadratic easeInOut function rather than
        // a linear function for the move.
        quarryRing.setNewTargetSize(targetQuarrySize,
            startTime: lastTime,
            speed: 600.0,
            easeAction: true)
    }
    
    
    /*
    *   Makes sure the jager ring starts off growing/shrinking towards its quarry.
    */
    private func changeJagerGrowDirection() {
        if quarryRing.ringTargetSize < jagerRing.ringSize {
            shrinkJagerRing()
        } else {
            growJagerRing()
        }
    }
    
    
    /*
    *   These two methods set up the resize action for the jager, moving it towards the 
    *   edge or the centre.
    */
    private func shrinkJagerRing() {
        jagerRing.setNewTargetSize(20, startTime: lastTime, speed: calcJagerGrowSpeed())
    }
    
    private func growJagerRing() {
        jagerRing.setNewTargetSize(Double(self.size.width), startTime: lastTime, speed: calcJagerGrowSpeed())
    }
    
    private func calcJagerGrowSpeed() -> Double {
        // Keeps getting faster, but is limited to 300. So max speed is reached at 60 
        // points. It's pretty darn fast then though.
        return (150.0+Double(gameData.score)*2.5).upperLimit(300.0)
    }
    
    
    private func isJagerTouchingQuarry() -> Bool {
        // So we work out if they are touching, based on whether or not the lines are
        // overlapping. We use the size (the diameter, not radius) and the line thickness.
        // Also, -1 to make it a little trickier. 
        let overlapSizeDiff = quarryRing.lineWidth + jagerRing.lineWidth - 1
        
        return abs(quarryRing.ringSize - jagerRing.ringSize) < Double(overlapSizeDiff)
            && quarryRing.isTargetSize // make sure quarry has finished growing
    }
    
    
    /*
    *   Scene building methods
    */
    
    private func buildSettingsMenu() {
        // We want the buttons to highlight the same colour as the bg
        let settingsLayer = SettingsLayer(gameScene: self, buttonHighlightColour: background.color)
        settingsLayer.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        settingsLayer.zPosition = 100
        addChild(settingsLayer)
        settingsLayer.growIn(withDuration: 0.15)
    }
    
    private func addGameLayer() {
        let blur = CIFilter(name: "CIGaussianBlur")
        blur!.setValue(10, forKey: kCIInputRadiusKey)
        gameLayer.filter = blur
        gameLayer.shouldEnableEffects = false
        gameLayer.shouldRasterize = true
        gameLayer.blendMode = .Alpha
        gameLayer.zPosition = 10
        
        addChild(gameLayer)
    }
    
    private func addScoreLabel() {
        scoreLabel.text = "0"
        scoreLabel.fontColor = GameColours.kGrey
        scoreLabel.fontSize = 46
        scoreLabel.verticalAlignmentMode = .Center
        scoreLabel.horizontalAlignmentMode = .Center
        scoreLabel.position = CGPoint(x: self.size.width/2, y: self.size.height - 36)
        gameLayer.addChild(scoreLabel)
    }
    
    private func addBestScoreLabel() {
        // Set up positions first
        let highScoreLabelTextPos = CGPoint(x: 22, y: self.size.height - 36)
        let highScoreLabelPos = CGPoint(x: 77, y: self.size.height - 36)
        
        // We need to cater for the potential problem in the InitialReady gamestate.
        // The buttons/labels may already be there, so just move them back into place.
        if highScoreLabel.parent == self.gameLayer {
            highScoreLabel.removeAllActions()
            highScoreLabel.runAction(SKAction.moveTo(highScoreLabelPos, duration: 0.1))
            
            highScoreLabelText.removeAllActions()
            highScoreLabelText.runAction(SKAction.moveTo(highScoreLabelTextPos, duration: 0.1))
            
            return
        }
        
        if gameData.newHighScore {
            highScoreLabelText.text = "NEW BEST!"
            highScoreLabelText.fontSize = 19
        } else {
            highScoreLabelText.text = "BEST"
            highScoreLabelText.fontSize = 23
            
            highScoreLabel.text = String(gameData.highScore)
            highScoreLabel.fontColor = GameColours.kGrey
            highScoreLabel.fontSize = 23
            highScoreLabel.verticalAlignmentMode = .Center
            highScoreLabel.horizontalAlignmentMode = .Left
            highScoreLabel.position = highScoreLabelPos
            gameLayer.addChild(highScoreLabel)
            highScoreLabel.flyIn(withDuration: 0.1, delay:0.1, direction:.Top)
        }
        
        highScoreLabelText.fontColor = GameColours.kWhite
        highScoreLabelText.verticalAlignmentMode = .Center
        highScoreLabelText.horizontalAlignmentMode = .Left
        highScoreLabelText.position = highScoreLabelTextPos
        gameLayer.addChild(highScoreLabelText)
        highScoreLabelText.flyIn(withDuration: 0.1, delay:0.1, direction:.Top)
    }
    
    private func addButtons() {
        // Set up positions first
        let gcButtonPos = CGPoint(x: self.size.width - 36, y: self.size.height - 36)
        let settingsButtonPos = CGPoint(x: self.size.width - 78, y: self.size.height - 36)
        
        // We need to cater for the potential problem in the InitialReady gamestate.
        if gcButton.parent == self.gameLayer {
            gcButton.removeAllActions()
            gcButton.runAction(SKAction.moveTo(gcButtonPos, duration: 0.1))
            
            settingsButton.removeAllActions()
            settingsButton.runAction(SKAction.moveTo(settingsButtonPos, duration: 0.1))
            
            return
        }
        
        // Game Centre Button
        gcButton.position = gcButtonPos
        gcButton.buttonImage.color = GameColours.kWhite
        gcButton.buttonImage.colorBlendFactor = 1.0
        gameLayer.addChild(gcButton)
        gcButton.flyIn(withDuration: 0.1, delay:0.1, direction:.Top)
        
        // Settings Button
        settingsButton.position = settingsButtonPos
        settingsButton.buttonImage.color = GameColours.kWhite
        settingsButton.buttonImage.colorBlendFactor = 1.0
        gameLayer.addChild(settingsButton)
        settingsButton.flyIn(withDuration: 0.1, delay:0.1, direction:.Top)
    }
    
    private func removeBestScoreLabel() {
        if !gameData.newHighScore {
            highScoreLabel.flyOut(withDuration: 0.2, direction:.Top)
        }
        highScoreLabelText.flyOut(withDuration: 0.2, direction:.Top)
    }
    
    private func removeButtons() {
        gcButton.flyOut(withDuration: 0.2, direction:.Top)
        settingsButton.flyOut(withDuration: 0.2, direction:.Top)
    }

    
    /*
    *   MARK: Button Actions
    */
    
    private func gameCentrePb() {
        GameManager.sharedInstance.showGameCentre()
        
        // Log all button usage
        Flurry.logEvent("Game Centre Button Pressed")
    }
    
    private func settingsPb() {
        gameLayer.shouldEnableEffects = true
        buildSettingsMenu()
        
        Flurry.logEvent("Settings Button Pressed")
    }
    
    
    /*
    *   MARK: EASTER EGG - GOSLING MODE
    *
    *   If the user taps a paradiddle, it will activate gosling mode! This is just a pic
    *   of Ryan Gosling in the centre of the game, that will change to a 'shocked' face 
    *   when the ring closes in on him. Ridiculous.
    */
    
    private func checkGoslingModeActivated(touches: Set<UITouch>) {
        // Add the touch side to the array:
        for touch in touches {
            let point = touch.locationInNode(self)
            if point.x < self.size.width/2 - 20 {
                goslingTaps += "L"
            } else if point.x > self.size.width/2 + 20 {
                goslingTaps += "R"
            }
        }
        
        // Gosling activated if sequence is a paradiddle (e.g. LRLLRLRRLRLLRLRR)
        let paradiddleString = "LRLLRLRRLRLLRLRR"
        
        // trim the gosling taps from front
        if goslingTaps.characters.count > paradiddleString.characters.count {
            goslingTaps.removeAtIndex(goslingTaps.startIndex)
        }
        
        //print("Gosling Taps: " + goslingTaps)
        
        // If they've tapped a paradiddle, and their score is < 2 (we don't want to be
        // getting in the way of a massive score), turn on gosling mode!
        if goslingTaps == paradiddleString && gameData.score < 2 {
            if goslingModeActivated {
                deactivateGoslingMode()
            } else {
                activateGoslingMode()
            }
            
            // reset goslingTaps to ensure a full paradiddle to deactivate
            goslingTaps = ""
        }
    }
    
    private func activateGoslingMode() {
        goslingModeActivated = true
        addGosling(true)
        
        // And log in Flurry!
        Flurry.logEvent("Gosling Mode Activated!")
    }
    
    private func deactivateGoslingMode() {
        goslingModeActivated = false
        removeGosling(true)
    }
    
    private func removeGosling(animated: Bool = false) {
        if let gos = gameLayer.childNodeWithName("gos") {
            if animated {
                gos.growOut(withDuration: 0.2)
            } else {
                gos.removeFromParent()
            }
        }
    }
    
    private func addGosling(animated: Bool = false) {
        let gos: SKSpriteNode
        
        if quarryRing.ringSize > Double(goslingShockedLimit) {
            gos = SKSpriteNode(imageNamed: "gos1")
            goslingShocked = false
        } else {
            gos = SKSpriteNode(imageNamed: "gos2")
            goslingShocked = true
        }
        
        gos.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        gos.name = "gos"
        gameLayer.addChild(gos)
        
        if animated {
            gos.growIn(withDuration: 0.2)
        }
    }
    
    private func reloadGosling() {
        removeGosling()
        addGosling()
    }
}
