//
//  GCHelper.m
//  CatRace
//
//  Created by Ray Wenderlich on 4/23/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "GCHelper.h"
//#import "AppDelegate.h"

@implementation GCHelper

@synthesize gameCenterAvailable;
@synthesize uiViewController = m_uiViewController;


#pragma mark - Initialization

+ (GCHelper *)sharedInstance
{
    static GCHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[GCHelper alloc] init];
    });
    
    return sharedInstance;
}


- (BOOL)isGameCenterAvailable
{
	// check for presence of GKLocalPlayer API
	Class gcClass = (NSClassFromString(@"GKLocalPlayer"));
	
	// check if the device is running iOS 4.1 or later
	NSString *reqSysVer = @"4.1";
	NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
	BOOL osVersionSupported = ([currSysVer compare:reqSysVer 
                                           options:NSNumericSearch] != NSOrderedAscending);
	
	return (gcClass && osVersionSupported);
}

- (id)init
{
    if ((self = [super init]))
    {
        m_gameCenterAvailable = [self isGameCenterAvailable];
        if (m_gameCenterAvailable)
        {
            NSNotificationCenter *nc = 
            [NSNotificationCenter defaultCenter];
            [nc addObserver:self 
                   selector:@selector(authenticationChanged) 
                       name:GKPlayerAuthenticationDidChangeNotificationName 
                     object:nil];
        }
    }
    return self;
}

#pragma mark Internal functions

- (void)authenticationChanged
{
    if ([GKLocalPlayer localPlayer].isAuthenticated && !m_userAuthenticated)
    {
       NSLog(@"Authentication changed: player authenticated.");
       m_userAuthenticated = TRUE;
    }
    else if (![GKLocalPlayer localPlayer].isAuthenticated && m_userAuthenticated)
    {
       NSLog(@"Authentication changed: player not authenticated");
       m_userAuthenticated = FALSE;
    }
                   
}

#pragma mark User functions

- (void)authenticateLocalUser
{
    if( m_gameCenterAvailable )
    {
        NSLog(@"Authenticating local user...");
        if ([GKLocalPlayer localPlayer].authenticated == NO)
        {
            [[GKLocalPlayer localPlayer] authenticateWithCompletionHandler:nil];        
        }
        else
        {
            NSLog(@"Already authenticated!");
        }
    }
}


- (void)reportScore:(int64_t)s
{
    if( m_gameCenterAvailable )
    {
        if( [GKLocalPlayer localPlayer].isAuthenticated )
        {
            GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:kLeaderboardIDScore];
            score.value = s;
            
            [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error)
            {
                if (error != nil)
                {
                    NSLog(@"%@", [error localizedDescription]);
                }
            }];
        }
    }
}


- (void)showLeaderboard
{
    GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
    
    gcViewController.gameCenterDelegate = self;

    gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
    gcViewController.leaderboardIdentifier = kLeaderboardIDScore;

    [m_uiViewController presentViewController:gcViewController animated:YES completion:nil];
}

- (void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [m_uiViewController dismissModalViewControllerAnimated: YES];
}




@end
