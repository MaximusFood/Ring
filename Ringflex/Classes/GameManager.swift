//
//  GameManager.swift
//  Ringflex
//
//  Created by Maxim Arnold on 12/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

/*
*   Singleton to handle stuff like IAPs, GC stuff, and so on.
*/

class GameManager : NSObject, IAPHelperObserver, AdManagerObserver, UIAlertViewDelegate {
    static let sharedInstance = GameManager()
    
    private(set) var adsRemoved = false
    private var numGames = 0
    private var showRemoveAdsAlert = false
    
    /*
    *   MARK: Initialisation
    */
    
    // This is a singleton, so init is private. Use sharedInstance to get teh GameManager.
    private override init() {
        // load adsRemoved
        // Using NSUserDefaults is very easy to hack. Jailbreakers get to have no ads for
        // free :)
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let ar = userDefaults.valueForKey("adsRemoved") as? Bool {
            adsRemoved = ar
        }
        
        if let ng = userDefaults.valueForKey("numGames") as? Int {
            numGames = ng
        }
        
        super.init()
        
        // Register self as IAPHelper observer
        IAPHelper.sharedInstance().addAdRemovalObserver(self)
        
        // Register self as an AdManagerObserver
        AdManager.sharedInstance().addAdManagerObserver(self)
    }
    
    /*
    *   MARK: Public Methods
    */
    
    func loadHighScore() -> Int {
        var highscore = 0
        
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let hs = userDefaults.valueForKey("highscore") as? Int {
            highscore = hs
        }
        
        return highscore
    }
    
    /*
    *   This is only called when the score achieved in-game is higher than the stored
    *   high score. This means that it is safe to use NSUserDefaults, but that if someone
    *   achieves a highscore while not connected to game centre that it will never be 
    *   reported.
    */
    func persistHighScore(score: Int) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setInteger(score, forKey: "highscore")
    }
    
    
    func logNewGame() {
        numGames++
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setInteger(numGames, forKey: "numGames")
        reportNumGames()
    }
    
    
    func showGameCentre() {
        GCHelper.sharedInstance().showLeaderboard()
    }
    
    
    /*
    *   MARK: IAPAdRemovalListener Methods
    *   GameManager is an ad removal listener to actually turn off the admanager, and 
    *   and store the purchase in NSUserDefaults. I know this is v. hackable, but I'm not
    *   bothered.
    */
    
    @objc func removeAdsPurchased() {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setBool(true, forKey: "adsRemoved")
        
        adsRemoved = true
        
        // And turn the AdManager off
        AdManager.sharedInstance().suppressAllAds = adsRemoved
    }
    
    
    /*
    *   MARK: UIAlertViewDelegate Methods
    */

    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch(buttonIndex) {
        case 0: //"No" pressed
            break
        case 1: //"Yes" pressed
            IAPHelper.sharedInstance().tapsRemoveAds()
            Flurry.logEvent("Remove Ads Button Pressed")
            break
        default:
            break
        }
    }

    
    /*
    *   MARK: AdManagerObserver Methods
    */
    
    func adDismissed() {
        if showRemoveAdsAlert && !adsRemoved {
            let alert = UIAlertView(title: "Remove Ads?",
                message: "Go on, support the developers!",
                delegate: GameManager.sharedInstance,
                cancelButtonTitle: "Nah",
                otherButtonTitles: "Yeh!")
            
            alert.show()
            
            showRemoveAdsAlert = false
        }
    }
    
    
    /*
    *   MARK: Private Helpers
    */
    
    /*
    *   Sends some Flurry events when users have played a certain number of games. If
    *   unable to connect to the net, Flurry caches the data and sends it at the first
    *   opportunity. Boom.
    */
    func reportNumGames() {
        if numGames == 1000 {
            Flurry.logEvent("1000 Games Played")
            showRemoveAdsAlert = true
        } else if numGames == 500 {
            Flurry.logEvent("500 Games Played")
            showRemoveAdsAlert = true
        } else if numGames == 250 {
            Flurry.logEvent("250 Games Played")
            showRemoveAdsAlert = true
        } else if numGames == 100 {
            Flurry.logEvent("100 Games Played")
            showRemoveAdsAlert = true
        } else if numGames == 50 {
            Flurry.logEvent("50 Games Played")
            showRemoveAdsAlert = true
        }
    }
}

