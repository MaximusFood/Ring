//
//  IAPHelper.h
//  v0.2.3
//
//  Created by Maxim Arnold on 12/11/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>


#define kRemoveAdsProductIdentifier @"com.maximarnold.ringflex.removeads"


@protocol IAPHelperObserver

- (void)removeAdsPurchased;

@end


@interface IAPHelper : NSObject <SKProductsRequestDelegate, SKPaymentTransactionObserver>
{
    BOOL m_areAdsRemoved;
    
    NSMutableArray<id<IAPHelperObserver>>* m_observers;
}


+ (IAPHelper *)sharedInstance;

- (void)restore;
- (void)tapsRemoveAds;

- (void)addAdRemovalObserver:(id<IAPHelperObserver>) observer;
- (void)removeAdRemovalObserver:(id<IAPHelperObserver>) observer;

@end

